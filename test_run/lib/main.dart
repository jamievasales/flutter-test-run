import 'package:flutter/material.dart';

class Cleaner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      color: Colors.greenAccent,
      height: 400,
      width: 300,
      child: Center(
        child: Text("Test",
            style: TextStyle(fontStyle: FontStyle.italic, fontSize: 40)),
      ),
    ));
  }
}

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: Text("App Bar"),
          ),
          body: Cleaner())));
}
